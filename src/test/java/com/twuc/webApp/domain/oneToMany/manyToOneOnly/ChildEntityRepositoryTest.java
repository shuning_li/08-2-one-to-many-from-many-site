package com.twuc.webApp.domain.oneToMany.manyToOneOnly;

import com.twuc.webApp.domain.ClosureValue;
import com.twuc.webApp.domain.JpaTestBase;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;

import java.util.Optional;

import static org.junit.jupiter.api.Assertions.*;

class ChildEntityRepositoryTest extends JpaTestBase {

    @Autowired
    private ChildEntityRepository childEntityRepository;

    @Autowired
    private ParentEntityRepository parentEntityRepository;

    @Test
    void should_save_parent_and_child_entity() {
        // TODO
        //
        // 请书写测试存储一对 one-to-many 的 child-parent 关系。并从 child 方面进行查询来证实存储已经
        // 成功。
        //
        // <--start-
        ClosureValue<Long> childIdValue = new ClosureValue<>();
        flushAndClear(em -> {
            ChildEntity child = childEntityRepository.save(new ChildEntity("child"));
            ParentEntity parent = parentEntityRepository.save(new ParentEntity("parent"));
            child.setParentEntity(parent);

            childIdValue.setValue(child.getId());
        });

        run(em -> {
            ChildEntity child = childEntityRepository.getOne(childIdValue.getValue());
            assertNotNull(child.getParentEntity());
        });
        // --end-->
    }

    @Test
    void should_remove_the_child_parent_relationship() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 解除 child 和 parent 的关系
        // Then child 和 parent 仍然存在，但是 child 不再引用 parent。
        //
        // <--start-
        ClosureValue<Long> childIdValue = new ClosureValue<>();
        ClosureValue<Long> parentIdValue = new ClosureValue<>();
        flushAndClear(em -> {
            ChildEntity child = childEntityRepository.save(new ChildEntity("child"));
            ParentEntity parent = parentEntityRepository.save(new ParentEntity("parent"));
            child.setParentEntity(parent);

            childIdValue.setValue(child.getId());
            parentIdValue.setValue(parent.getId());
        });

        flushAndClear(em -> {
            ChildEntity child = childEntityRepository.getOne(childIdValue.getValue());
            child.removeParent();
        });

        run(em -> {
            ChildEntity child = childEntityRepository.getOne(childIdValue.getValue());
            ParentEntity parent = parentEntityRepository.getOne(parentIdValue.getValue());
            assertNull(child.getParentEntity());
            assertNotNull(parent);
        });
        // --end-->
    }

    @Test
    void should_remove_child_and_parent() {
        // TODO
        //
        // 请书写测试：
        //
        // Given 一对 one-to-many 的 child-parent 关系。
        // When 删除 child 和 parent。
        // Then child 和 parent 不再存在。
        //
        // <--start-
        ClosureValue<Long> childIdValue = new ClosureValue<>();
        ClosureValue<Long> parentIdValue = new ClosureValue<>();
        flushAndClear(em -> {
            ChildEntity child = childEntityRepository.save(new ChildEntity("child"));
            ParentEntity parent = parentEntityRepository.save(new ParentEntity("parent"));
            child.setParentEntity(parent);

            childIdValue.setValue(child.getId());
            parentIdValue.setValue(parent.getId());
        });

        flushAndClear(em -> {
            parentEntityRepository.deleteById(parentIdValue.getValue());
            childEntityRepository.deleteById(childIdValue.getValue());
        });


        run(em -> {
            Optional<ParentEntity> optionalParent = parentEntityRepository.findById(parentIdValue.getValue());
            Optional<ChildEntity> optionalChild = childEntityRepository.findById(childIdValue.getValue());

            assertFalse(optionalChild.isPresent());
            assertFalse(optionalParent.isPresent());
        });
        // --end-->
    }
}